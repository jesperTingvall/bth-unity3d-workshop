﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int life;

    public void Damage(int damage) {
        life -= damage;

        if (life <= 0) {
            Destroy(gameObject);
            FindObjectOfType<PlayerScript>().Score++;
        }
    }
}
