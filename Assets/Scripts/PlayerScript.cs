﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    public CharacterController controller;
    public float speed;
    public float turnSpeed;

    public GameObject bullet;
    public Transform BulletSpawn;

    private Animator animator;

    [HideInInspector] // Hides score in editor
    public int Score; 

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () {
        float forward = Input.GetAxis("Vertical");
        float turn = Input.GetAxis("Horizontal");

        animator.SetFloat("Speed", Mathf.Abs( forward));

        controller.SimpleMove(transform.forward * speed * forward);
        transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);

        if (Input.GetButtonDown("Fire1"))
        {
            Instantiate(bullet, BulletSpawn.position, BulletSpawn.rotation);
            animator.SetTrigger("Fire");

        }
        Debug.Log(Score);
	}
}