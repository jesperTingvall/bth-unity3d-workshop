﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonBall : MonoBehaviour {

    private Rigidbody rigidbody;
    public float Speed;

	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = Speed * transform.forward;
        Destroy(gameObject, 5); // Destroy the canon ball after 5 second.
	}

    public void OnCollisionEnter(Collision collision) {
        var enemy = collision.gameObject.GetComponent<Enemy>();

        if (enemy)
        {
            enemy.Damage(1);
            Destroy(gameObject);
        }
    }
}